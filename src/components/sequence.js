export default [
  {
    start: 15.325803,
    end: 18.869954,
    type: 'Time',
  },
  {
    start: 18.7,
    end: 22.386225,
    type: 'SlideText',
  },
  {
    start: 18.6,
    end: 18.7,
    type: 'Unlock',
    width: 41.4,
    paddingTop: 15,
    top: 87.3,
    left: 29.9,
  },
  {
    start: 19.786227,
    end: 22.386225,
    type: 'Area',
    width: 15.4,
    paddingTop: 15.6,
    top: 21.3,
    left: 30.9,
    href: 'https://screenlife.tech/alisa-app?vkmessages=short#/photo',
    metrik:{
      name:'ClickIconSocial',
      payload:{ClickIconSocialName:'photo'}
    }
  }, //photo
  {
    start: 19.786227,
    end: 22.386225,
    type: 'Area',
    width: 15.4,
    paddingTop: 15.6,
    top: 33.8,
    left: 7.8,
    href: 'https://instagram.com/misha_druzhnov?igshid=57z5zxm0iz55',
    external: true,
    metrik:{
      name:'ClickIconSocial',
      payload:{ClickIconSocialName:'instagram'}
    }
  }, // inst
  {
    start: 19.786227,
    end: 22.386225,
    type: 'Area',
    width: 15.6,
    paddingTop: 15.6,
    top: 89.3,
    left: 30.6,
    href: 'https://screenlife.tech/alisa-app?vkmessages=short#/whatsapp',
    metrik:{
      name:'ClickIconSocial',
      payload:{ClickIconSocialName:'whatsapp'}
    }
  }, // whatsapp
  {
    start: 19.786227,
    end: 22.386225,
    type: 'Area',
    width: 15.6,
    paddingTop: 15.6,
    top: 89.3,
    left: 76.6,
    href: 'https://screenlife.tech/alisa-app?vkmessages=short#/vk',
    metrik:{
      name:'ClickIconSocial',
      payload:{ClickIconSocialName:'vk'}
    }
  }, // vk
  // {
  //   start: 19.786227,
  //   end: 22.386225,
  //   type: 'Area',
  //   width: 15.6,
  //   paddingTop: 15.5,
  //   top: 8.8,
  //   left: 76.4,
  //   scale: 2,
  //   href: 'https://www.youtube.com/channel/UC7DpXb-WwNcm1TK3QyFjmOg',
  //   external: true,
  //   metrik:{
  //     name:'ClickIconSocial',
  //     payload:{ClickIconSocialName:'youtube'}
  //   }
  // }, // youtube
  {
    start: 21,
    end: 22.386225,
    type: 'Pause',
    timeout: 5000,
  },

  {
    start: 65.271874,
    end: 66.471858,
    type: 'Area',
    width: 31,
    paddingTop: 13,
    top: 11,
    left: 36,
    href: 'https://ad.admitad.com/g/49218b26566a099e887019ac1b781b',
    pause: 2000,
    metrik:{
      name:'ClickToyRu',
      payload:{ClickToyRuPage:'https://ad.admitad.com/g/49218b26566a099e887019ac1b781b'}
    }
  }, //toy ru
  // {
  //   start: 65.271874,
  //   end: 66.471858,
  //   type: 'Area',
  //   width: 83,
  //   paddingTop: 25,
  //   top: 38,
  //   left: 14,
  //   href: 'https://www.toy.ru/catalog/toys-figurki/roblox_10774_mashina_s_figurkami_jailbreak_swat_unit/',
  // }, //toy ru car
  // {
  //   start: 65.271874,
  //   end: 66.471858,
  //   type: 'Area',
  //   width: 83,
  //   paddingTop: 27,
  //   top: 69,
  //   left: 14,
  //   href:
  //     'https://www.toy.ru/catalog/toys-figurki/roblox_rob0200_figurka_geroya_simoon68_golden_god_core_s_aksessuarami/',
  // }, //toy ru toy
  {
    start: 80.093455,
    end: 83.133707,
    type: 'Area',
    width: 83,
    paddingTop: 25,
    top: 38,
    left: 14,
    href: 'https://ad.admitad.com/g/49218b26566a099e887019ac1b781b',
    metrik:{
      name:'ClickToyRu',
      payload:{ClickToyRuPage:'https://ad.admitad.com/g/49218b26566a099e887019ac1b781b'}
    }
  }, //toy ru car
  {
    start: 114.553416,
    end: 115.253416,
    type: 'ClickToContinue',
    width: 70,
    paddingTop: 12,
    top: 84,
    left: 4,
  },
  {
    start: 137.835937,
    end: 143.447638,
    type: 'Area',
    width: 54,
    paddingTop: 12,
    top: 4,
    left: 18,
    href:
      'https://screenlife.tech/alisa-app/#/vk/messages/%D0%90%D0%BB%D0%B8%D1%81%D0%B0%20%D0%A1%D0%B5%D0%BB%D0%B5%D0%B7%D0%BD%D0%B5%D0%B2%D0%B0',
      metrik:{
        name:'ClickVkDialogSelezneva',
      }
  }, //alisa

  
  {
    start: 220.053124,
    end: 221.857065,
    type: 'Area',
    width: 68,
    paddingTop: 23,
    top: 30,
    left: 4,
    href: 'https://screenlifer.com/interactive/kak-pomoch-dikim-zhivotnym-interaktivnyj-obzor/ ',
    metrik:{
      name:'ClickLinkScreenlifer',
    }
  }, //screenlife

  {
    start: 223.99777,
    // start: 0,
    end: 224.1,
    type: 'Game',
  },
  {
    start: 353.409208,
    end: 354.409205,
    type: 'Pause',
    timeout: 5000,
  },
  {
    start: 353.409208,
    end: 354.409205,
    type: 'Area',
    width: 15.4,
    paddingTop: 15.6,
    top: 21.3,
    left: 30.9,
    href: 'https://screenlife.tech/alisa-app/#/photo',
    metrik:{
      name:'ClickIconSocial',
      payload:{ClickIconSocialName:'photo'}
    }
  }, //photo
  {
    start: 353.409208,
    end: 354.409205,
    type: 'Area',
    width: 15.4,
    paddingTop: 15.6,
    top: 33.8,
    left: 7.8,
    href: 'https://instagram.com/misha_druzhnov?igshid=57z5zxm0iz55',
    external: true,
    metrik:{
      name:'ClickIconSocial',
      payload:{ClickIconSocialName:'instagram'}
    }
  }, // inst
  {
    start: 353.409208,
    end: 354.409205,
    type: 'Area',
    width: 15.6,
    paddingTop: 15.6,
    top: 89.3,
    left: 30.6,
    href: 'https://screenlife.tech/alisa-app/#/whatsapp',
    metrik:{
      name:'ClickIconSocial',
      payload:{ClickIconSocialName:'whatsapp'}
    }
  }, // whatsapp
  {
    start: 353.409208,
    end: 354.409205,
    type: 'Area',
    width: 15.6,
    paddingTop: 15.6,
    top: 89.3,
    left: 76.6,
    href: 'https://screenlife.tech/alisa-app/#/vk',
    metrik:{
      name:'ClickIconSocial',
      payload:{ClickIconSocialName:'vk'}
    }
  }, // vk
  // {
  //   start: 353.409208,
  //   end: 354.409205,
  //   type: 'Area',
  //   width: 15.6,
  //   paddingTop: 15.5,
  //   top: 8.8,
  //   left: 76.4,
  //   scale: 2,
  //   href: 'https://www.youtube.com/channel/UC7DpXb-WwNcm1TK3QyFjmOg',
  //   external: true,
  //   metrik:{
  //     name:'ClickIconSocial',
  //     payload:{ClickIconSocialName:'youtube'}
  //   }
  // }, // youtube

  {
    start: 395.725487,
    end: 403.968353,
    type: 'Time',
  },
]
